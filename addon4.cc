#include <sstream>
#include <napi.h>

class AsyncWorker : public Napi::AsyncWorker {
public:
  static Napi::Value Create(const Napi::CallbackInfo& info) {

    if (info.Length() != 1) {
      return Reject(info.Env(), "MissingArgument");
    } else if (!info[0].IsString()) {
      return Reject(info.Env(), "InvalidArgument");
    }

    std::string input = info[0].As<Napi::String>().Utf8Value();
    AsyncWorker* worker = new AsyncWorker(info.Env(), input);
    worker->Queue();
    return worker->deferredPromise.Promise();
  }

protected:
  static Napi::Value Reject(Napi::Env env, const char* msg) {
    Napi::Promise::Deferred failed = Napi::Promise::Deferred::New(env);
    failed.Reject(Napi::Error::New(env, msg).Value());
    return failed.Promise();
  }

  void Execute() override {
    if(input.size() < 1) {
      SetError("EmptyName");
      return;
    }

    std::stringstream str;
    str << "hello, " << input;

    result = str.str();
  }

  virtual void OnOK() override {
      deferredPromise.Resolve(Napi::String::New(Env(), result));
  }

  virtual void OnError(const Napi::Error& e) override {
      deferredPromise.Reject(e.Value());
  }

private:
  AsyncWorker(napi_env env, std::string& name) :
    Napi::AsyncWorker(env),
    input(name),
    result(),
    deferredPromise(Napi::Promise::Deferred::New(env)) { }

  std::string input;
  std::string result;

  Napi::Promise::Deferred deferredPromise;
};

Napi::Object Init(Napi::Env env, Napi::Object exports) {
  return Napi::Function::New(env, AsyncWorker::Create);
}
NODE_API_MODULE(addon, Init)
