const { expect } = require('chai');

const { addon3 } = require('..');

describe('#addon3', () => {
  it('calls back', () => {
    addon3(function(msg){
      expect(msg).to.deep.equal('hello world');
    });
  });

});
