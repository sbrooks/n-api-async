const { expect } = require('chai');

const { addon } = require('..');

describe('#addon', () => {
  it('resolves the promise with a successful result', async () => {
    const result = await addon(4);
    expect(result).to.deep.equal(2);
  });

  it('rejects a promise with no input', async () => {
    try {
      await addon();
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/A number was expected/);
    }
  });

  it('rejects a promise with invalid input', async () => {
    try {
      await addon('X');
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/A number was expected/);
    }
  });

  it('rejects a promise with a failed result', async () => {
    try {
      await addon(-1);
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/NegativeInput/);
    }
  });
});
