const { expect } = require('chai');

const { addon2 } = require('..');

describe('#addon2', () => {
  it('returns the expected result result', () => {
    const result = addon2.hello('foo');
    expect(result).to.deep.equal('hello, foo');
  });

  it('throws for missing input', () => {
    try {
      addon2.hello();
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/Wrong number of arguments/);
    }
  });


  it('throws for invalid input', () => {
    try {
      addon2.hello(3.14);
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/Expected first argument to be a string/);
    }
  });
});
