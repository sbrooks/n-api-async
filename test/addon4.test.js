const { expect } = require('chai');

const { addon4 } = require('..');

describe('#addon4', () => {
  it('resolves the promise with a successful result', () => {
    addon4('world').then(result => {
      expect(result).to.deep.equal('hello, world');
    });
  });

  it('async resolves the promise with a successful result', async () => {
    const result = await addon4('world');
    expect(result).to.deep.equal('hello, world');
  });

  it('rejects a promise with no input', () => {
    addon4().then(()=>{
      throw new Error('Expected method to throw');
    }).catch(err=> {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/MissingArgument/);
    });
  });

  it('rejects a promise with wrong input type', () => {
    addon4(3.14).then(() => {
      throw new Error('Expected method to throw');
    }).catch(err => {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/InvalidArgument/);
    });
  });

  it('rejects a promise with invalid input', () => {
    addon4('').then(()=>{
      throw new Error('Expected method to throw');
    }).catch(err=>{
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/EmptyName/);
    })
  });

  it('async rejects a promise with no input', async () => {
    try {
      await addon4();
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/MissingArgument/);
    }
  });

  it('async rejects a promise with wrong input type', async () => {
    try {
      await addon4(3.14);
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/InvalidArgument/);
    }
  });

  it('async rejects a promise with invalid input', async () => {
    try {
      await addon4('');
      throw new Error('Expected method to throw');
    } catch(err) {
      expect(err)
        .to.be.an('Error')
        .with.property('message')
        .match(/EmptyName/);
    }
  });
});

