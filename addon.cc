#include <cmath>

#include <napi.h>

class AsyncWorker : public Napi::AsyncWorker {
public:
  static Napi::Value Create(const Napi::CallbackInfo& info) {
    AsyncWorker* worker = new AsyncWorker(info.Env());

    double x = info[0].As<Napi::Number>().DoubleValue();

    if(x < 0) {
      worker->SetError("NegativeInput");
    } else {
      worker->_input = x;
    }

    worker->Queue();
    return worker->_deferred.Promise();
  }

protected:
  void Execute() override {
    // Can also call SetError here if desired
    this->_result = std::sqrt(this->_input);
  }

  virtual void OnOK() override {
      _deferred.Resolve(Napi::Number::New(Env(), _result));
  }

  virtual void OnError(const Napi::Error& e) override {
      _deferred.Reject(e.Value());
  }

private:
  AsyncWorker(napi_env env) : Napi::AsyncWorker(env), _input(0), _result(0), _deferred(Napi::Promise::Deferred::New(env)) { }

  double _input;
  double _result;

  Napi::Promise::Deferred _deferred;
};

Napi::Object Init(Napi::Env env, Napi::Object exports) {
  return Napi::Function::New(env, AsyncWorker::Create);
}

NODE_API_MODULE(addon, Init)
